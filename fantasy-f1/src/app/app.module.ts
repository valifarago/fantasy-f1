import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DriversComponent } from './home/drivers/drivers.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { RacesComponent } from './home/races/races.component';

import { HighchartsChartComponent } from 'highcharts-angular';
import { D3jsChartComponent } from './home/races/d3js-chart/d3js-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    DriversComponent,
    HomeComponent,
    RacesComponent,
    HighchartsChartComponent,
    D3jsChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
