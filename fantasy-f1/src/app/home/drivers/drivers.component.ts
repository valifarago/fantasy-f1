import { Component } from '@angular/core';
import { HttpService } from '../../http.service';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent {
  drivers: [];
  selectedDrivers: [
    {
      name: string
    },
    {
      name: string
    }
  ];
  constructor(
    private _http: HttpService
  ) { }

  ngOnInit() {
    this._http.fetchDrivers().subscribe(data => {
      this.drivers = data['MRData'].DriverTable.Drivers;
    });
  }
  getSelectedDrivers() {
    this.selectedDrivers = [
      {
        name: localStorage.getItem('firstDriversName')
      },
      {
        name: localStorage.getItem('secondDriversName')
      }
    ]
    return this.selectedDrivers;
  }
}
