import { Component, OnInit, Output } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  public firstDriver: number;
  public secondDriver: number;
  public firstDriversName: string = '';
  public secondDriversName: string = '';
  public drivers: [];
  public inputValue: string;

  constructor(
    private _http: HttpService
  ) { }

  ngOnInit() {
    this._http.fetchDrivers().subscribe(data => {
      this.drivers = data['MRData'].DriverTable.Drivers;
    });
  }

  getRandom1stDriverNumber() {
    // @ts-ignore
    this.inputValue = document.getElementById('choosenDriver1').value;
    let permanentNumbers = this.drivers.map(driver => driver['permanentNumber']);
    let randomIndex = Math.floor(Math.random() * 20);
    this.inputValue
      ? this.drivers.filter(driver => driver['familyName'] === this.inputValue ? this.firstDriver = driver['permanentNumber'] : '')
      : this.firstDriver = permanentNumbers[randomIndex]

    this.getFirstDriverName();
    return this.firstDriver;
  }


  getFirstDriverName() {
    // @ts-ignore
    this.inputValue = document.getElementById('choosenDriver1').value;
    this.inputValue
      ? this.firstDriversName = this.inputValue
      : this.drivers.filter(driver => {
        if (driver['permanentNumber'] === this.firstDriver) {
          this.firstDriversName = driver['familyName'];
        }
      });
    this.showAlert();
    localStorage.setItem('firstDriversName', this.firstDriversName);
    return this.firstDriversName;
  }

  getRandom2ndDriverNumber() {
    // @ts-ignore
    this.inputValue = document.getElementById('choosenDriver2').value;
    let permanentNumbers = this.drivers.map(driver => driver['permanentNumber']);
    let randomIndex2 = Math.floor(Math.random() * 20);
    this.inputValue
      ? this.drivers.filter(driver => driver['familyName'] === this.inputValue ? this.secondDriver = driver['permanentNumber'] : '')
      : this.secondDriver = permanentNumbers[randomIndex2]

    this.getSecondDriverName();
    return this.secondDriver;
  }

  getSecondDriverName() {
    // @ts-ignore
    this.inputValue = document.getElementById('choosenDriver2').value;
    this.inputValue
      ? this.secondDriversName = this.inputValue
      : this.drivers.filter(driver => {
        if (driver['permanentNumber'] === this.secondDriver) {
          this.secondDriversName = driver['familyName'];
        }
      });
    this.showAlert();
    localStorage.setItem('secondDriversName', this.secondDriversName);
    return this.secondDriversName;
  }

  showAlert() {
    if (this.firstDriversName === 'Hamilton' || this.secondDriversName === 'Hamilton') {
      alert('You should really choose someone else instead of Hamilton ;)')
    }
  }
}
