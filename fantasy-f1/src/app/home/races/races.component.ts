import { Component, OnInit, Output } from '@angular/core';
import { HttpService } from '../../http.service';

import * as Highcharts from 'highcharts';
import * as _ from 'lodash';

@Component({
  selector: 'app-races',
  templateUrl: './races.component.html',
  styleUrls: ['./races.component.scss']
})
export class RacesComponent implements OnInit {
  public drivers: [];
  points = [25, 18, 15, 12, 10, 8, 6, 4, 2, 1];
  @Output() public allRace: {};
  numberOfRaces: number = 1;
  data: {}
  highcharts = Highcharts;
  updateFlag: boolean
  chartOptions = {
    chart: {
      type: "bar",
      events: {
        redraw: true
      }
    },
    title: {
      text: "Races"
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: "Points"
      }
    },
    legend: {
      enabled: false
    },
    series: []

  };
  constructor(
    private _http: HttpService
  ) { }

  ngOnInit() {
    this._http.fetchDrivers().subscribe(data => {
      this.drivers = data['MRData'].DriverTable.Drivers;
    })
  }

  displaySelectedDrivers() {
    document.getElementById('firstDriverOnRacePage').innerHTML = localStorage.getItem('firstDriversName');
    document.getElementById('secondDriverOnRacePage').innerHTML = localStorage.getItem('secondDriversName');
  }

  getMultipleRaces(number: number) {
    number = this.numberOfRaces;
    let generatedRaces = [];
    let race = [];
    let mergedRaces = [];

    for (let i = 0; i < number; i++) {
      let races = this.drivers.sort(() => .5 - Math.random()).slice(0, 10);
      race = races.map((race, i) => {
        if (races[i]['familyName'] === race['familyName']) {
          return {
            // @ts-ignore
            ...races[i],
            name: race['familyName'],
            data: [this.points[i]]
          }
        }
      });
      generatedRaces.push(race);
    }

    // @ts-ignore
    mergedRaces = [].concat(generatedRaces.flat(1));

    // @ts-ignore
    this.allRace = mergedRaces.sort((a, b) => a['permanentNumber'] - b['permanentNumber']);

    const groupedRaces = _.groupBy(this.allRace, 'name');

    // points
    let allPoints = []
    for (const group of Object.values(groupedRaces)) {
      let points = []
      // @ts-ignore
      for (let i = 0; i < group.length; i++) {
        const element = group[i];

        points.push(element.data)
      }
      // @ts-ignore
      points = points.flat(1);
      allPoints.push(points);
    }

    // racers
    // @ts-ignore
    let allRacer = this.allRace.map((name: { familyName: any; }) => name.familyName).filter((function (value: any, index: any, self: { indexOf: (arg0: any) => void; }) {
      return self.indexOf(value) === index;
    }))
    let finalResult = allRacer.map((uniqe: any, i: string | number) => {
      return {
        name: uniqe,
        data: allPoints[i].reduce((acc: any, i: any) => { return acc + i }, 0)
      }
    })

    this.allRace = finalResult;
    this.generateChart(this.allRace);
    this.displaySelectedDrivers();
    return this.allRace;
  }

  private generateChart(races) {
    races = this.allRace;
    let convertedValues = races.map(race => [race.name, race.data]);
    this.updateFlag = true;

    this.chartOptions.series = [{
      dataSorting: {
        enabled: true
      },
      dataLabels: {
        enabled: true,
      },
      data: convertedValues
    }]
  }
}