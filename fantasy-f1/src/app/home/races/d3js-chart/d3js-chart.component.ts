import { Component, OnInit, Input } from '@angular/core';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';

@Component({
  selector: 'app-d3js-chart',
  templateUrl: './d3js-chart.component.html',
  styleUrls: ['./d3js-chart.component.scss']
})
export class D3jsChartComponent implements OnInit {
  @Input() public allRace: [];
  private width: number;
  private height: number;
  private margin = { top: 20, right: 20, bottom: 30, left: 40 };
  private x: any;
  private y: any;
  private svg: any;
  private g: any;
  constructor() { }

  ngOnInit() {
    this.initSvg();
    this.initAxis();
    this.drawAxis();
    this.drawBars();
  }

  ngOnChanges() {
    this.updateChart()
  }

  private initSvg() {
    this.svg = d3.select('svg.svg');
    this.width = +this.svg.attr('width') - this.margin.left - this.margin.right;
    this.height = +this.svg.attr('height') - this.margin.top - this.margin.bottom;
    this.g = this.svg
      .append('g')
      .attr(
        'transform',
        'translate(' + this.margin.left + ',' + this.margin.top + ')'
      );
  }

  private initAxis() {
    this.x = d3Scale
      .scaleBand()
      .rangeRound([0, this.width])
      .padding(0.1);
    this.y = d3Scale.scaleLinear().rangeRound([this.height, 0]);
    this.x.domain(this.allRace.map(d => d['name']));
    this.y.domain([0, d3Array.max(this.allRace, d => parseInt(d['data'], 10))]);
  }

  private drawAxis() {
    this.g
      .append('g')
      .attr('class', 'axis axis - x')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(d3Axis.axisBottom(this.x));
    this.g
      .append('g')
      .attr('class', 'axis axis - y')
      .call(d3Axis.axisLeft(this.y))
      .append('text')
      .attr('class', 'axis-title')
      .attr('transform', 'rotate(-90)')
      .attr('y', -40)
      .attr('x', -200)
      .attr('dy', '0.71em')
      .text('Points');
  }

  private drawBars() {
    this.g
      .selectAll('.bar')
      .data(this.allRace)
      .enter()
      .append('rect')
      .attr('class', 'bar')
      .attr('x', d => this.x(d['name']))
      .attr('y', d => this.y(parseInt(d['data'], 10)))
      .attr('width', this.x.bandwidth())
      .attr('height', d => this.height - this.y(parseInt(d['data'], 10)));
  }

  public updateChart() {
    this.g
      .selectAll('.bar')
      .remove();
    this.g
      .selectAll('.axis')
      .remove();
    this.g
      .selectAll('.axis-title')
      .remove();
    this.initAxis();
    this.drawAxis();
    this.drawBars();
  }
}
