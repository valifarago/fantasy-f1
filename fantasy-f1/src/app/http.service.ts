import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  fetchDrivers() {
    return this.http.get('http://ergast.com/api/f1/2019/drivers.json')
  }
}
